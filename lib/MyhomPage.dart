import 'package:flutter/material.dart';
import 'package:posterapp/view.dart';
import 'package:posterapp/loadingEffict.dart';

import 'Widgets/SkeletonGrid.dart';
import 'Widgets/SkeletonList.dart';
import 'Widgets/SkeletonProfile.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          scrolledUnderElevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          title: Text(
            'Loading Effect',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.indigoAccent.withOpacity(0.7),
                      fixedSize:
                          Size(mediaQuery.width * 0.8, mediaQuery.width * 0.2)),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ViewLoading(
                              title: "Grid loading Skeleton",
                              body: SkeletonGrid(mediaQuery: mediaQuery),
                            )));
                  },
                  child: Text(
                    'Grid Skeleton',
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(color: Colors.white),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.indigoAccent,
                      fixedSize:
                          Size(mediaQuery.width * 0.8, mediaQuery.width * 0.2)),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ViewLoading(
                              title: " List loading Skeleton",
                              body: SkeletonList(mediaQuery: mediaQuery),
                            )));
                  },
                  child: Text(
                    'Skeleton List',
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(color: Colors.white),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.indigo,
                      fixedSize:
                          Size(mediaQuery.width * 0.8, mediaQuery.width * 0.2)),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ViewLoading(
                              title: "profile loading Skeleton",
                              body: SkeletonProfile(mediaQuery: mediaQuery),
                            )));
                  },
                  child: Text(
                    'Skeleton Profile',
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
