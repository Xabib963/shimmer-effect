import 'package:flutter/material.dart';
import 'package:posterapp/MyhomPage.dart';

class ViewLoading extends StatelessWidget {
  const ViewLoading({super.key, required this.body, required this.title});
  final Widget body;
  final String title;

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        scrolledUnderElevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: Text(
          title,
          style: Theme.of(context).textTheme.headlineLarge,
        ),
      ),
      body: body,
    );
  }
}
